April 22 2015 ALP 
-----------------

Feedback on exercise 2:

Award writing: should be unambiguous, final and complete

1. Heading
2. Title
3. Decisions
4. Reasons
5. Date/Signature

Heading 

eg: In the matter of an Arbitration under the International Arbitration Act (Cap 143A)
and under the Rules of the Singapore International Arbitration Cenre (2013)

Between
Tug Marine Pte Ltd 
              .... Claimant
Glow
              .... Respondent

Title


-----------------------
 Directions (or Orders)
-----------------------

Decisions
1. Amendments
2. Change of hearing date
3. Costs

1. The Claimant has leave to amend its Reply and Defence to COunterclaim by adding the followign paragrpahs, and to serve its amended pleading on the Respondent and filt it with the Tribunal by 20 april 2015:

"by an assignment dated 15 July ....."

2. Any costs which have been or may be incurred as a result of the Claimants application and these Direction shall be costs in the arbitations

[OR - The Claimants shall bear the cost occasioned by the above amendments with such costs to be determined by the Tribunal if not agreed between the parties.]

3. The oral hearing remains as fixed from 12 to 16 May 2015 (5 days)

[OR - The oral hearing fixed from 12 to 16 May 2015 shall be vacated and reschedules on dates to be determined by the Tribunal after consulting with the parties.]


Reasons
Leave to Amend Pleadings

- SIAC Rule 17.5:
  "A party may amend ..."
- Not a question of power, but discretion
- Deal of Respondents 5 objections:
   1. No need for amendment - substance of case relates to collaspse of Plarform No 3 - incident with R says was caused by C
   2. Notice of assignment - annex a - irrelevant - unrelated to mattes pleasded
   3. Assignemen - annex b - relevant only to r's financing arrangement
   4. Lateness -> delay
   5. Bank's consent - R's action/counterclaim - effective/relevant?

- Fundament issue - is R properper party to bring counterclaim?
- Maintain status quo on merits - relevant can be fully argued in due course - each side much eb given a full opportunity to present its case (points #1, 2, 3 and 5)
- Lateness (poinrt 4) - practicalilty (will delay necessarily follow?)/prejudice?


Costs
- Discretion - to be exercised judically - not arbitrarily
- Must deal speficially with R's application for indemnity costs. Genrally only where on party solely to blame - not here - R failed to give proper discovery / make proper disclsure of documents!
- costs in arbitration (ie loser pays) == fair

Hearing Dates

- SIAC Rule 16.1
"The tribunal shall conduct the arbitration ..."
- at present, not necessary to vacate
- Remember that parties always hae liberty to apply!

Date/Signature

====================================

At this stage do not need to decide on substantive points.

For practicum:
a) Identify points of agreement - Listen carefully. 
b)  

For exam: write the award decision first.

=======================================
For Exam:
---------

3 hr closed book
3 essay questions

variations on the theme
more thinking time.
Be familiar with principles
- avoid disapproportionete time on questions

Issues span the length of the course

what are the big issues:
- juristiction <---- IMPORTANT. outline 3. who are parties? be able to quote main question
   section 10 IAA amended in 2012 permits court to review of both positive and negative
   findings of tribunal on jurisdiction
   page 7 or outline 3 shows contrasting approach on uk court vs singapre court of appeal
    - one is non-model and other is model law. different philospohy.
-  who reviews jurisdiction -> the court.
-  if you need support on powers -> turn to the high court. coverts tribunal order to court order 
- relationship between tribunal and court is important
- 5 cases in workshop - relations of arbirtation to courts, 
  eg attempts to set aside awards - the part II http://gcia2015.pbworks.com/w/page/95276291/Part%20II%20Recourse%20Against%20Awards%20%28pages%207-13%29

- Just international arbirtation it is not specific to Singapore.
- arbitrators dites and powers, relationshsip bet ar and court, costs: look at question as a whole and not duplicate what is being said. BE SPECIFC in answering questions
