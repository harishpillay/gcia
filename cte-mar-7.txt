gcia-contract-slides.pdf

slide17a:

Illegality 2

- restraint of trade 
 - generally invalid
  - trade secret, customer relationship, stable work force

viciting factors - related to events/fact during
discharge - after the case

slide 17b:
severable contracts: can the contract be divided into parts

discharge by breach: looking for an option to exit

conditions are more important terms, warranty less important.

wait-and-see option: treating contract as subsisting

18a:
important area: FRUSTRATION
no fixed rules/situations and need to test "radical change in nature of
duty"
eg: destruction of subject matter, personal incapacity

"Frustration must not be self induced" - must be because of event outside
your control

govt interference: in the uk some land was meant to be a reservoir but
because of the 2nd world war, the land was acquired and so no reservoir
was built. so the contractor claimed frustration.

Frustracted Contract Act: all obligations discharged, monies piad are 
recoverable.

eg: rental of equipment and there is fire, you can claim frustration.
court can adjust based on benefits received and expenses incurred
because of the use of asset before the frustration.

18b:
force majeure
- written into the contract
- frustration is not written in but the court decides

19a:
remedies:
 - rights when contract is broken; specific performance if it is about land
 - injunction: prevent doing something.

equitable remedies

"factual matrix" - look at facts of each case and not be rigid on rules

Damages: monetary compensation for loss not punishment;
    in law suit, the damanges is divided into 2: liability and quantum

19b:
Damages: - the aim of damages is to put innocent pary in same position as if
contract was carried out.

Types of losses: 
expectation - calculate on basis of contracted fully carried
out without breaches
reliance: wasted expenses

Impt case: Hadley v Baxendale
  - a shaft in a mill was supposed to be repaired but it was not sent on
    time for repair so can there be a claim of loss of profits. court said
    that there was not knowledge that there wasn't a spare shaft available.
  - abnormal loss: unexpected situations

  - Chaplin v Hicks: court can assess probabilities eg: Starwood vs Asia Hotels

Mitigation: 20a:

- take reasonable steps

two types of clubs: proprietary club and members club.
members club own the property, proprietary club is owned by someone else.

comparables: use info from other clubs that had price changes and average is
  about the same, then it is OK.

liquidated damages: must be genuine estimate of loss. it should not be a penalty

Limitation: time limit to sue is 6 years from date of BREACH not contract.

Robertson Quay case (RobertsonVsSteen): issue was about interest payable. page 4: 
para 27, 28 (important ones), read through para 30. did not show causation.
in page 12: from para 38 and especially para 39. 
para 50: talks about normal losses and abnormal losses
exam questions: extra lucrative contract - you supply something special but
did not provide and so loss of profit. words like "extra" suggests something
that needed to be mentioned.

para 80 and 82 83.

RBC vs Defu:
- insufficient information
- para 2 (section 2(1) of misrepresenation act)

case is important because it looks at s.2(1) of misrep act.
this case discusses this misrep as if fradulent
- fradulant: lying, reckless, cheat
in this case it was negligent. 
fradulent: liable for all loses exen it is unexpected loses
negligent: less serious - this case looks at in page 28 
court held that it was an innocent misrepresentation:
remedy for innocent misrep is indemnity not damages
indemnity - claims for MUST spend expenses.
read para 114 onwards.

----

GCIA Law of Evidence

think of conveyor belt


Hearsay - section 62

principle of cross examination can't be applied in a hearsay situation

